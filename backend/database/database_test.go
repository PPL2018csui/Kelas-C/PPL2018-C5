package database_test

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	d "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/database"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

type DatabaseSuite struct {
	suite.Suite
	database d.Database
}

func TestDatabaseSuite(t *testing.T) {
	suite.Run(t, &DatabaseSuite{})
}

func (ds *DatabaseSuite) SetupSuite() {
	client, _ := gorm.Open("sqlite3", "test.sqlite")

	ds.database = d.New(client)

	// migrate mock database
	ds.database.Migrate(&m.Product{})
	ds.database.Migrate(&m.Category{})
	ds.database.Migrate(&m.Image{})
	ds.database.Migrate(&m.Price{})
	ds.database.Migrate(&m.Catalog{})

	// insert dummy product
	dummyProduct := m.Product{
		Code:     "default_p",
		Name:     "dummy_setup",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct2 := m.Product{
		Code:     "default_p2",
		Name:     "dummy_setup2",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct, _ = ds.database.CreateProduct(dummyProduct)
	dummyProduct2, _ = ds.database.CreateProduct(dummyProduct2)

	dummyCatalog := m.Catalog{
		Name:      "Cat-test",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  []m.Product{dummyProduct},
	}
	dummyCatalog2 := m.Catalog{
		Name:      "Cat-test2",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  nil,
	}

	ds.database.CreateCatalog(dummyCatalog)
	ds.database.CreateCatalog(dummyCatalog2)
}

func (ds *DatabaseSuite) TearDownSuite() {
	os.Remove("test.sqlite")
}

func (ds *DatabaseSuite) TestCreateProductOnSuccess() {
	dummyProduct := m.Product{
		Code:     "kode",
		Category: m.Category{Name: "kategori"},
	}

	_, err := ds.database.CreateProduct(dummyProduct)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveProductByIDOnNotFound() {
	_, err := ds.database.RetrieveProductByID(0)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveProductByIDOnSuccess() {
	_, err := ds.database.RetrieveProductByID(1)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestUpdateProductOnNotFound() {
	dummyProduct := m.Product{
		ID: 0,
	}

	_, err := ds.database.UpdateProduct(dummyProduct)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestUpdateProductOnSuccess() {
	dummyProduct := m.Product{
		ID:   1,
		Name: "bukan_dummy_setup",
	}

	_, err := ds.database.UpdateProduct(dummyProduct)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestDeleteProductOnNotFound() {
	_, err := ds.database.DeleteProduct(0)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestDeleteProductOnSuccess() {
	_, err := ds.database.DeleteProduct(2)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveAllProductOnNotFound() {
	dummyPage := m.Pagination{Offset: 100, Limit: -5}

	_, err := ds.database.RetrieveAllProduct(dummyPage)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveAllProductOnSuccess() {
	dummyPage := m.Pagination{Offset: 0, Limit: 5}

	_, err := ds.database.RetrieveAllProduct(dummyPage)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveProductByUploadIDOnNotFound() {
	dummyPage := m.Pagination{Offset: 100, Limit: -5}
	dummyUploadID := uint(123)

	_, err := ds.database.RetrieveProductByUploadID(dummyUploadID, dummyPage)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveProductByUploadIDOnSuccess() {
	dummyPage := m.Pagination{Offset: 0, Limit: 5}
	dummyUploadID := uint(123)

	_, err := ds.database.RetrieveProductByUploadID(dummyUploadID, dummyPage)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveAllCategoryOnNotFound() {
	dummyPage := m.Pagination{Offset: 100, Limit: -5}

	_, err := ds.database.RetrieveAllCategory(dummyPage)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveAllCategoryOnSuccess() {
	dummyPage := m.Pagination{Offset: 0, Limit: 5}

	_, err := ds.database.RetrieveAllCategory(dummyPage)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveCategoryByIDOnNotFound() {
	_, err := ds.database.RetrieveCategoryByID(0)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveCategoryByIDOnSuccess() {
	_, err := ds.database.RetrieveCategoryByID(2)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestUpdateCategoryOnNotFound() {
	dummyCategory := m.Category{
		ID: 0,
	}

	_, err := ds.database.UpdateCategory(dummyCategory)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestUpdateCategoryOnSuccess() {
	dummyCategory := m.Category{
		ID:   2,
		Name: "bukan_dummy_setup",
	}

	_, err := ds.database.UpdateCategory(dummyCategory)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestDeleteCategoryOnNotFound() {
	_, err := ds.database.DeleteCategory(0)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestDeleteCategoryOnSuccess() {
	_, err := ds.database.DeleteCategory(1)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestCreateCatalogOnProductError() {
	dummyProduct := m.Product{
		Code:     "cat_pro",
		Name:     "cat_pro",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct2 := m.Product{
		Code:     "cat_pro_2",
		Name:     "cat_pro_2",
		Category: m.Category{Name: "default_k"},
	}

	dummyCatalog := m.Catalog{
		Name:      "Cat-1",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  []m.Product{dummyProduct, dummyProduct2},
	}

	_, err := ds.database.CreateCatalog(dummyCatalog)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestCreateCatalogOnDateError() {
	dummyProduct := m.Product{
		Code:     "cat_pro",
		Name:     "cat_pro",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct2 := m.Product{
		Code:     "cat_pro_2",
		Name:     "cat_pro_2",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct, _ = ds.database.CreateProduct(dummyProduct)
	dummyProduct2, _ = ds.database.CreateProduct(dummyProduct2)

	t, _ := time.Parse(time.RFC3339, "0000-00-00T00:00:00Z")
	dummyCatalog := m.Catalog{
		Name:      "Cat-1",
		StartTime: t,
		Products:  []m.Product{dummyProduct, dummyProduct2},
	}

	_, err := ds.database.CreateCatalog(dummyCatalog)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestCreateCatalogOnSuccess() {
	dummyProduct := m.Product{
		Code:     "cat_pro",
		Name:     "cat_pro",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct2 := m.Product{
		Code:     "cat_pro_2",
		Name:     "cat_pro_2",
		Category: m.Category{Name: "default_k"},
	}

	dummyProduct, _ = ds.database.CreateProduct(dummyProduct)
	dummyProduct2, _ = ds.database.CreateProduct(dummyProduct2)

	dummyCatalog := m.Catalog{
		Name:      "Cat-1",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  []m.Product{dummyProduct, dummyProduct2},
	}

	_, err := ds.database.CreateCatalog(dummyCatalog)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveCatalogByIDOnNotFound() {
	_, err := ds.database.RetrieveCatalogByID(0)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveCatalogByIDOnSuccess() {
	_, err := ds.database.RetrieveCatalogByID(1)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestRetrieveAllCatalogOnError() {
	dummyPage := m.Pagination{Offset: 100, Limit: -5}

	_, err := ds.database.RetrieveAllCatalog(dummyPage)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestRetrieveAllCatalogOnSuccess() {
	dummyPage := m.Pagination{Offset: 1, Limit: 5}

	_, err := ds.database.RetrieveAllCatalog(dummyPage)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestDeleteCatalogOnNotFound() {
	_, err := ds.database.DeleteCatalog(0)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestDeleteCatalogOnSuccess() {
	_, err := ds.database.DeleteCatalog(2)

	assert.Nil(ds.T(), err, "Error should be nil!")
}

func (ds *DatabaseSuite) TestUpdateCatalogOnNotFound() {
	dummyCatalog := m.Catalog{
		ID:        5,
		Name:      "Cat-test-updated",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  nil,
	}

	_, err := ds.database.UpdateCatalog(dummyCatalog)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestUpdateCatalogOnProductNotFound() {
	dummyProduct := m.Product{
		Code: "cat_pro",
		Name: "cat_pro",
	}
	dummyCatalog := m.Catalog{
		ID:        1,
		Name:      "Cat-test-updated",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  []m.Product{dummyProduct},
	}

	_, err := ds.database.UpdateCatalog(dummyCatalog)

	assert.NotNil(ds.T(), err, "Error should not be nil!")
}

func (ds *DatabaseSuite) TestUpdateCatalogOnSuccess() {
	dummyProduct := m.Product{
		Code: "cat_pro",
		Name: "cat_pro",
	}
	dummyProduct, _ = ds.database.CreateProduct(dummyProduct)
	dummyCatalog := m.Catalog{
		ID:        1,
		Name:      "Cat-test-updated",
		StartTime: time.Now(),
		EndTime:   time.Now(),
		Products:  []m.Product{dummyProduct},
	}

	_, err := ds.database.UpdateCatalog(dummyCatalog)

	assert.Nil(ds.T(), err, "Error should be nil!")
}
