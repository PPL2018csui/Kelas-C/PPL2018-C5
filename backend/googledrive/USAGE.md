### Installation Guide

#### Google Apps Creation

- Use this [wizard](https://console.developers.google.com/flows/enableapi?apiid=drive) to create or select a project in the Google Developers Console and automatically turn on the API. Click **Continue**, then **Go to credentials**.
- On the **Add credentials to your project*) page, click the **Cancel** button.
- At the top of the page, select the **OAuth consent screen** tab. Select an **Email address**, enter a **Product name** if not already set, and click the **Save** button.
- Select the **Credentials** tab, click the **Create credentials** button and select **OAuth client ID**.
- Select the application type **Other**, enter the name "Sikatan", and click the **Create** button.
- Click **OK** to dismiss the resulting dialog.
- Click the **Download JSON** button to the right of the client ID.
- Add the string to the GoogleDriveOpt as `ClientSecret`
- to get `Oauth2Token`, you have to call `GetOauth2Token` method. run `token_fetcher.go <your-client-secret>` from your cli.
- You're asked to open certain link on your browser and allow access for Sikatan apps to your google drive.
- paste the given code after allowing access to your cli and the program will continue generating token.
- Add the string to the GoogleDriveOpt as `Oauth2Token`

#### Google Script Deployment

- Use this [wizard](https://script.google.com/) to create google script
- Open the script editor and paste `script.gs`
- On the script editor do File -> Manage Versions -> Save New Version
- On the script editor do Publish -> Deploy as Web App
- At execute the app as, select "your account"
- At Who has access to the app, select "Anyone, even anonymous"
- Click "Deploy"
- Copy "Current web app URL"
- Click "OK"
- Add the ID part of the string (`https://script.google.com/macros/s/<ID>/exec`) to the GoogleDriveOpt as `GScriptID`

#### Parallel Resource Fetching Config
- Dont forget to add `NumWorker` on the GoogleDriveOpt to utilize goroutine for parallel resource fetching mechanism
