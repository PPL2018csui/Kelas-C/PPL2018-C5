package handler

import (
	"encoding/csv"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	c "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/controller"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

// handler holds the structure for Handler
type handler struct {
	controller c.Controller
}

// Handler holds the contract for Handler
type Handler interface {
	// Ping should handle healthcheck in top level routing
	Ping(*gin.Context)

	// CreateProductByCSV should handle the request for uploading products through CSV
	// It gives response as json string with the default structure
	CreateProductByCSV(*gin.Context)
	// RetrieveProductByID should handle the request for retrieving product by given ID
	// It gives response as json string with the default structure
	RetrieveProductByID(*gin.Context)
	// RetrieveProductByUploadID should handle the request for retrieving product by given UploadID
	// It gives response as json string with the default structure
	RetrieveProductByUploadID(ctx *gin.Context)
	// RetrieveAllProduct should handle the request for retrieving all product that match the query
	// It gives response as json string with the default structure
	RetrieveAllProduct(*gin.Context)
	// DeleteProduct should handle the request for deleting product by given ID
	// It gives response as json string with the default structure
	DeleteProduct(*gin.Context)

	// Createcatalog should handle the request for creating catalog
	// It gives response as json string with the default structure
	CreateCatalog(*gin.Context)
	// RetrieveCatalogByID should handle the request for retrieving catalog by given ID
	// It gives response as json string with the default structure
	RetrieveCatalogByID(*gin.Context)
	// RetrieveAllCatalog should handle the request for retrieving catalog by pagination
	// It gives response as json string with the default structure
	RetrieveAllCatalog(*gin.Context)
	// UpdateCatalog should handler the request for updating existing catalog
	// It gives response as json string with the default structure
	UpdateCatalog(*gin.Context)
	// DeleteCatalog should handle the request for deleting catalog
	// It gives response as json string with the default structure
	DeleteCatalog(*gin.Context)

	GetImage(*gin.Context)
}

// response define default structure of handler response
type response struct {
	// Meta contains metadata of the response
	Meta meta
	// Body contains content of response
	Body interface{}
}

// New is a function for creating handler
func New(controller c.Controller) Handler {
	return &handler{
		controller: controller,
	}
}

// Ping if a function for handling healthcheck in top level routing
func (h *handler) Ping(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	resp := &response{metaSuccess, "Hello worldv2!"}
	ctx.JSON(200, resp)

	return
}

// CreateProductByCSV for handling the request of bulk upload product via CSV file upload
func (h *handler) CreateProductByCSV(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	f, err := ctx.FormFile("file")
	if err != nil {
		m := metaFileNotFound
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	file, _ := f.Open()

	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		m := metaFileParseError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	upload_id := uint(time.Now().Unix())
	var products []m.Product
	for _, line := range lines[1:] {

		product := m.Product{
			Category:    m.Category{Name: line[0]},
			UploadID:    upload_id,
			Code:        line[1],
			Name:        line[2],
			Description: line[3],
			Specs:       line[4],
			Weight:      line[5],
			Length:      line[6],
			Width:       line[7],
			Height:      line[8],
			Brand:       line[9],
			Price: m.Price{
				PriceZoneA: line[10],
				PriceZoneB: line[11],
				BonusZoneA: line[12],
				BonusZoneB: line[13],
			},
			Status:            line[14],
			Stock:             line[15],
			AlertLevel:        line[16],
			StopLevel:         line[17],
			NeedMemberAddress: line[18],
		}

		products = append(products, product)
	}

	h.controller.BulkCreateProduct(ctx, products, upload_id)

	resp := &response{metaSuccess, upload_id}
	ctx.JSON(201, resp)

	return
}

// RetrieveProductByID is a function for handling the request for retrieving product by given ID
func (h *handler) RetrieveProductByID(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		m := metaInvalidQuery
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	product, err := h.controller.RetrieveProductByID(ctx, uint(id))
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, product}
	ctx.JSON(200, resp)

	return
}

// RetrieveProductByUploadID is a function for handling the request for retrieving product by given ID
func (h *handler) RetrieveProductByUploadID(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	queryLimit := ctx.DefaultQuery("limit", "10")
	queryOffset := ctx.DefaultQuery("offset", "0")

	limit, err1 := strconv.Atoi(queryLimit)
	offset, err2 := strconv.Atoi(queryOffset)
	uploadID, err3 := strconv.Atoi(ctx.Param("id"))
	if err1 != nil || err2 != nil || err3 != nil {
		m := metaInvalidQuery
		m.Error = "Wrong Limit, Offset or UploadID Type. Should be int"
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	page := m.Pagination{Limit: limit, Offset: offset}

	products, err := h.controller.RetrieveProductByUploadID(ctx, uint(uploadID), page)
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, products}
	ctx.JSON(200, resp)

	return
}

// RetrieveAllProduct is a function for handling the request for retrieving all product that match the query
func (h *handler) RetrieveAllProduct(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	keyword, keyExist := ctx.GetQuery("keyword")
	filter, filterExist := ctx.GetQueryArray("filter")
	queryLimit := ctx.DefaultQuery("limit", "10")
	queryOffset := ctx.DefaultQuery("offset", "0")

	query := m.Query{}

	if keyExist {
		query.Keyword = keyword
	}
	if filterExist {
		query.Filter = filter
	}

	limit, err1 := strconv.Atoi(queryLimit)
	offset, err2 := strconv.Atoi(queryOffset)
	if err1 != nil || err2 != nil {
		m := metaInvalidQuery
		m.Error = "Wrong Limit or Offset Type"
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	query.Pagination = m.Pagination{Limit: limit, Offset: offset}

	products, err := h.controller.RetrieveAllProduct(ctx, query)
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, products}
	ctx.JSON(200, resp)

	return
}

// DeleteProduct is a function for handling the request for deleting product by given ID
func (h *handler) DeleteProduct(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		m := metaInvalidQuery
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	product, err := h.controller.DeleteProduct(ctx, uint(id))
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, product}
	ctx.JSON(200, resp)

	return
}

// Createcatalog is a function for handling the request for creating catalog
func (h *handler) CreateCatalog(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	var catalog m.Catalog
	err := ctx.ShouldBindJSON(&catalog)
	if err != nil {
		m := metaJSONDecodeError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	catalog, err = h.controller.CreateCatalog(ctx, catalog)
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, catalog}
	ctx.JSON(201, resp)

	return
}

// RetrieveCatalogByID is a function for handling the request for retrieving catalog by given ID
func (h *handler) RetrieveCatalogByID(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		m := metaInvalidQuery
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	catalog, err := h.controller.RetrieveCatalogByID(ctx, uint(id))

	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, catalog}
	ctx.JSON(200, resp)

	return
}

// RetrieveAllCatalog is a function for handling request for retrieving catalog by pagination
func (h *handler) RetrieveAllCatalog(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	queryLimit := ctx.DefaultQuery("limit", "10")
	queryOffset := ctx.DefaultQuery("offset", "0")

	limit, err1 := strconv.Atoi(queryLimit)
	offset, err2 := strconv.Atoi(queryOffset)
	if err1 != nil || err2 != nil {
		m := metaInvalidQuery
		m.Error = "Wrong Limit or Offset Type"
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	pagination := m.Pagination{Limit: limit, Offset: offset}

	catalogs, err := h.controller.RetrieveAllCatalog(ctx, pagination)
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, catalogs}
	ctx.JSON(200, resp)

	return
}

// UpdateCatalog is a function for handling request for updating existing catalog
func (h *handler) UpdateCatalog(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	var catalog m.Catalog
	err := ctx.ShouldBindJSON(&catalog)
	if err != nil {
		m := metaJSONDecodeError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	catalog, err = h.controller.UpdateCatalog(ctx, catalog)
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, catalog}
	ctx.JSON(200, resp)

	return
}

// DeleteCatalog is a function for handling request for deleting existing catalog
func (h *handler) DeleteCatalog(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		m := metaInvalidQuery
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(400, resp)
		return
	}

	catalog, err := h.controller.DeleteCatalog(ctx, uint(id))
	if err != nil {
		m := metaControllerError
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	resp := &response{metaSuccess, catalog}
	ctx.JSON(200, resp)

	return
}

func (h *handler) GetImage(ctx *gin.Context) {
	select {
	case <-ctx.Request.Context().Done():
		m := metaContextTimeout
		resp := &response{m, nil}
		ctx.JSON(408, resp)
		return
	default:
	}

	filename := ctx.Param("filename")
	if _, err := os.Stat("static/" + filename); err != nil {
		m := metaFileNotFound
		m.Error = err.Error()
		resp := &response{m, nil}
		ctx.JSON(500, resp)
		return
	}

	buffer, _ := ioutil.ReadFile("static/" + filename)
	contentType := http.DetectContentType(buffer)
	ctx.Data(200, contentType, buffer)
	return
}
