package main

import (
	"os"

	g "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/googledrive"
)

func main() {
	if len(os.Args) != 2 {
		panic("Plase supply one argument for client secret")
	}
	secret := os.Args[1]
	g.GetOauth2Token(secret)
}
