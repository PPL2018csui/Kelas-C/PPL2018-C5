package controller_test

import (
	"context"
	"errors"

	e "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/elasticsearch"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

type DBMock struct{}

func (dm *DBMock) Migrate(i interface{}) {
	return
}

func (dm *DBMock) CreateProduct(product m.Product) (m.Product, error) {
	if product.ID == 12 {
		return m.Product{}, errors.New("Product ID doesn't exist!")
	}
	return product, nil
}

func (dm *DBMock) RetrieveAllProduct(page m.Pagination) (m.Products, error) {
	if page.Limit < 0 {
		return m.Products{}, errors.New("Error while Querying from database!")
	}
	return m.Products{}, nil
}

func (dm *DBMock) RetrieveProductByID(id uint) (m.Product, error) {
	if id > 900 {
		return m.Product{}, errors.New("Product ID doesn't exist!")
	}
	return m.Product{}, nil
}

func (dm *DBMock) RetrieveProductByUploadID(uploadid uint, page m.Pagination) (m.Products, error) {
	if uploadid < 1524246131 {
		return m.Products{}, errors.New("Product Upload ID doesn't exist!")
	}
	return m.Products{}, nil
}

func (dm *DBMock) UpdateProduct(product m.Product) (m.Product, error) {
	if product.ID == 99 {
		return m.Product{}, errors.New("Product ID doesn't exist!")
	}
	return product, nil
}

func (dm *DBMock) DeleteProduct(id uint) (m.Product, error) {
	if id > 900 {
		return m.Product{}, errors.New("Product ID doesn't exist!")
	}
	return m.Product{ID: id}, nil
}

func (dm *DBMock) CreateCategory(category m.Category) (m.Category, error) {
	return m.Category{}, nil
}

func (dm *DBMock) RetrieveAllCategory(page m.Pagination) ([]m.Category, error) {
	return nil, nil
}

func (dm *DBMock) RetrieveCategoryByID(id uint) (m.Category, error) {
	return m.Category{}, nil
}

func (dm *DBMock) UpdateCategory(category m.Category) (m.Category, error) {
	return m.Category{}, nil
}

func (dm *DBMock) DeleteCategory(id uint) (m.Category, error) {
	return m.Category{}, nil
}

func (dm *DBMock) CreateCatalog(catalog m.Catalog) (m.Catalog, error) {
	if catalog.ID == 999 {
		return m.Catalog{}, errors.New("Product ID doesn't exist!")
	}

	return m.Catalog{}, nil
}

func (dm *DBMock) RetrieveCatalogByID(id uint) (m.Catalog, error) {
	if id > 99 {
		return m.Catalog{}, errors.New("Catalog ID doesn't exist!")
	}

	return m.Catalog{}, nil
}

func (dm *DBMock) RetrieveAllCatalog(page m.Pagination) ([]m.Catalog, error) {
	if page.Offset < 0 {
		return nil, errors.New("Range error!")
	}
	return nil, nil
}

func (dm *DBMock) UpdateCatalog(catalog m.Catalog) (m.Catalog, error) {
	if catalog.Name == "Err" {
		return m.Catalog{}, errors.New("Catalog not found")
	}
	return m.Catalog{}, nil
}

func (dm *DBMock) DeleteCatalog(id uint) (m.Catalog, error) {
	if id == 999 {
		return m.Catalog{}, errors.New("Catalog not found")
	}
	return m.Catalog{}, nil
}

type ESMock struct{}

func (esm *ESMock) IndexProduct(ctx context.Context, product m.Product) (e.ResponseStatus, error) {
	if product.ID == 13 {
		return "", errors.New("Indexing error!")
	}
	return e.ELASTIC_INDEX_DOC_RESPONSE, nil
}

func (esm *ESMock) UpdateProduct(ctx context.Context, product m.Product) (e.ResponseStatus, error) {
	if product.ID == 101 {
		return e.ELASTIC_NOT_FOUND_DOC_RESPONSE, errors.New("Product ID doesn't exist!")
	}
	return e.ELASTIC_UPDATE_DOC_RESPONSE, nil
}

func (esm *ESMock) DeleteProduct(ctx context.Context, product m.Product) (e.ResponseStatus, error) {
	if product.ID == 555 {
		return "", errors.New("Product delete error ES!")
	}
	return "", nil
}

func (esm *ESMock) RetrieveAllProduct(ctx context.Context, input m.Query) (m.Products, error) {
	if input.Keyword == "truk" {
		return m.Products{}, errors.New("Not Found")
	}
	return m.Products{}, nil
}

type GDMock struct{}

func (gdm *GDMock) RetrieveAllImage(ctx context.Context, product m.Product) {}
