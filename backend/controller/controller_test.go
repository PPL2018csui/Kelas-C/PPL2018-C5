package controller_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	c "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/controller"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

type ControllerSuite struct {
	suite.Suite
	controller c.Controller
}

func TestControllerSuite(t *testing.T) {
	suite.Run(t, &ControllerSuite{})
}

func (cs *ControllerSuite) SetupSuite() {
	db := &DBMock{}
	es := &ESMock{}
	gd := &GDMock{}

	cs.controller = c.New(c.ControllerOpt{Database: db, ElasticSearch: es, GoogleDrive: gd, NumWorker: 2})
}

func (cs *ControllerSuite) TestInstantiation() {
	db := &DBMock{}
	es := &ESMock{}
	gd := &GDMock{}

	controller := c.New(c.ControllerOpt{Database: db, ElasticSearch: es, GoogleDrive: gd, NumWorker: 2})

	assert.NotNil(cs.T(), controller, "Instance should not be nil!")
}

func (cs *ControllerSuite) TestBulkCreateProductOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	cs.controller.BulkCreateProduct(ctx, []m.Product{m.Product{ID: 1}, m.Product{ID: 2}}, 1524246131)
}

func (cs *ControllerSuite) TestBulkCreateProductOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	cs.controller.BulkCreateProduct(ctx, []m.Product{m.Product{ID: 12}, m.Product{ID: 2}}, 1524246131)
	time.Sleep(1 * time.Second)
}

func (cs *ControllerSuite) TestBulkCreateProductOnElasticSearchError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	cs.controller.BulkCreateProduct(ctx, []m.Product{m.Product{ID: 13}, m.Product{ID: 2}}, 1524246131)
	time.Sleep(1 * time.Second)
}

func (cs *ControllerSuite) TestBulkCreateProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	cs.controller.BulkCreateProduct(ctx, []m.Product{m.Product{ID: 1}, m.Product{ID: 2}}, 1524246131)
	time.Sleep(1 * time.Second)
}

func (cs *ControllerSuite) TestCreateProductOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.CreateProduct(ctx, m.Product{ID: 1})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestCreateProductOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, errs := cs.controller.CreateProduct(ctx, m.Product{ID: 12})

	assert.NotNil(cs.T(), errs, "Error should not be nil!")
}

func (cs *ControllerSuite) TestCreateProductOnElasticSearchError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, errs := cs.controller.CreateProduct(ctx, m.Product{ID: 13})

	assert.NotNil(cs.T(), errs, "Error should not be nil!")
}

func (cs *ControllerSuite) TestCreateProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, errs := cs.controller.CreateProduct(ctx, m.Product{ID: 1})

	assert.Nil(cs.T(), errs, "Error should be nil!")
}

func (cs *ControllerSuite) TestRetrieveAllProductOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.RetrieveAllProduct(ctx, m.Query{Pagination: m.Pagination{}})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveAllProductOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveAllProduct(ctx, m.Query{Pagination: m.Pagination{Offset: 10, Limit: -10}})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveAllProductOnSuccessPaginationOnly() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveAllProduct(ctx, m.Query{Pagination: m.Pagination{}})

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestRetrieveProductByIDOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.RetrieveProductByID(ctx, 123)

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveProductByIDOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveProductByID(ctx, 999)

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveProductByIDOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveProductByID(ctx, 123)

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestRetrieveProductByUploadIDOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.RetrieveProductByUploadID(ctx, 123, m.Pagination{Offset: 1, Limit: 5})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveProductByUploadIDOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveProductByUploadID(ctx, 999, m.Pagination{Offset: 1, Limit: 5})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveProductByUploadIDOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveProductByUploadID(ctx, 1524246131, m.Pagination{Offset: 1, Limit: 5})

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestDeleteProductOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.DeleteProduct(ctx, 123)
	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestUpdateProductOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.UpdateProduct(ctx, m.Product{})
	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestUpdateProductOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.UpdateProduct(ctx, m.Product{ID: 99})
	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestUpdateProductOnElasticSearchError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.UpdateProduct(ctx, m.Product{ID: 101})
	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestUpdateProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.UpdateProduct(ctx, m.Product{ID: 1})
	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestDeleteProductOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.DeleteProduct(ctx, 999)
	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestDeleteProductOnElasticSearchError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.DeleteProduct(ctx, 555)
	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestDeleteProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.DeleteProduct(ctx, 123)
	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestRetrieveAllProductOnSuccessWithQuery() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveAllProduct(ctx, m.Query{})

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestRetrieveAllProductOnError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveAllProduct(ctx, m.Query{Keyword: "truk"})

	assert.NotNil(cs.T(), err, "Error should not be nil")
}

func (cs *ControllerSuite) TestCreateCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.CreateCatalog(ctx, m.Catalog{})

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestCreateCatalogOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.CreateCatalog(ctx, m.Catalog{})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestCreateCatalogOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.CreateCatalog(ctx, m.Catalog{ID: 999})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveCatalogByIDOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.RetrieveCatalogByID(ctx, 1)
	assert.NotNil(cs.T(), err, "Error should not be nil")
}

func (cs *ControllerSuite) TestRetrieveCatalogByIDOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveCatalogByID(ctx, 1)
	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestRetrieveCatalogByIDOnFailed() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveCatalogByID(ctx, 999)
	assert.NotNil(cs.T(), err, "Error should not be nil")
}

func (cs *ControllerSuite) TestRetrieveAllCatalogOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.RetrieveAllCatalog(ctx, m.Pagination{Offset: 1, Limit: 5})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveAllCatalogOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveAllCatalog(ctx, m.Pagination{Offset: -1, Limit: 5})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestRetrieveAllCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.RetrieveAllCatalog(ctx, m.Pagination{Offset: 1, Limit: 5})

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestUpdateCatalogOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.UpdateCatalog(ctx, m.Catalog{})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestUpdateCatalogOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.UpdateCatalog(ctx, m.Catalog{Name: "Err"})

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestUpdateCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.UpdateCatalog(ctx, m.Catalog{})

	assert.Nil(cs.T(), err, "Error should be nil")
}

func (cs *ControllerSuite) TestDeleteCatalogOnContextTimeoutError() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	_, err := cs.controller.DeleteCatalog(ctx, 123)

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestDeleteCatalogOnDatabaseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.DeleteCatalog(ctx, 999)

	assert.NotNil(cs.T(), err, "Error should not be nil!")
}

func (cs *ControllerSuite) TestDeleteCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := cs.controller.DeleteCatalog(ctx, 123)

	assert.Nil(cs.T(), err, "Error should be nil")
}
