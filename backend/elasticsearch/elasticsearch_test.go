package elasticsearch_test

import (
	"context"
	"testing"
	"time"

	"net/http"
	"net/http/httptest"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	e "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/elasticsearch"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

func instantiate(payload string) (*httptest.Server, e.ElasticClient, error) {
	handler := http.NotFound
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler(w, r)
	}))

	if payload == INSTANTIATE_ERROR_INVOKER {
		_, err := e.New(e.ElasticOpt{IndexName: "index", TypeName: "type", HostURL: server.URL})

		return server, nil, err
	} else if payload == GENERAL_ERROR_RESPONSE {
		handler = func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(payload))
		}

		client, err := e.New(e.ElasticOpt{IndexName: "index", TypeName: "type", HostURL: server.URL})
		if err != nil {
			return server, nil, err
		}

		handler = http.NotFound
		return server, client, nil

	} else {
		handler = func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(payload))
		}

		client, err := e.New(e.ElasticOpt{IndexName: "index", TypeName: "type", HostURL: server.URL})
		if err != nil {
			return server, nil, err
		}

		return server, client, nil
	}

}

type ElasticSuite struct {
	suite.Suite
}

func TestElasticSuite(t *testing.T) {
	suite.Run(t, &ElasticSuite{})
}

func (es *ElasticSuite) TestInstantiateElasticSearchClientOnSuccess() {
	server, _, err := instantiate(ELASTIC_CONNECT_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")
}

func (es *ElasticSuite) TestInstantiateElasticSearchClientOnFailed() {
	server, _, err := instantiate(INSTANTIATE_ERROR_INVOKER)
	defer server.Close()

	assert.NotNil(es.T(), err, "Error should not be nil")
}

func (es *ElasticSuite) TestIndexProductOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	server, client, err := instantiate(INDEX_PRODUCT_SUCCESS_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.IndexProduct(ctx, m.Product{ID: 101})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")
}

func (es *ElasticSuite) TestIndexProductOnError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(GENERAL_ERROR_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.IndexProduct(ctx, m.Product{ID: 101})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")
}

func (es *ElasticSuite) TestIndexProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(INDEX_PRODUCT_SUCCESS_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	res, err := client.IndexProduct(ctx, m.Product{ID: 101})

	assert.Nil(es.T(), err, "Error should be nil")
	assert.Equal(es.T(), e.ELASTIC_INDEX_DOC_RESPONSE, res, "result should be `created`")
}

func (es *ElasticSuite) TestDeleteProductOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	server, client, err := instantiate(DELETE_PRODUCT_SUCCESS_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.DeleteProduct(ctx, m.Product{ID: 101})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")

}

func (es *ElasticSuite) TestDeleteProductOnError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(GENERAL_ERROR_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.DeleteProduct(ctx, m.Product{ID: 101})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")
}

func (es *ElasticSuite) TestDeleteProductOnNotFound() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(DELETE_PRODUCT_NOT_FOUND_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	res, err := client.DeleteProduct(ctx, m.Product{ID: 101})

	assert.Nil(es.T(), err, "Error should be nil")
	assert.Equal(es.T(), e.ELASTIC_NOT_FOUND_DOC_RESPONSE, res, "result should be `not_found`")
}

func (es *ElasticSuite) TestDeleteProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(DELETE_PRODUCT_SUCCESS_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	res, err := client.DeleteProduct(ctx, m.Product{ID: 101})

	assert.Nil(es.T(), err, "Error should be nil")
	assert.Equal(es.T(), e.ELASTIC_DELETE_DOC_RESPONSE, res, "result should be `deleted`")
}

func (es *ElasticSuite) TestUpdateProductOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	server, client, err := instantiate(UPDATE_PRODUCT_SUCCESS_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.UpdateProduct(ctx, m.Product{ID: 101})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")
}

func (es *ElasticSuite) TestUpdateProductOnError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(GENERAL_ERROR_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.UpdateProduct(ctx, m.Product{ID: 101})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")
}

func (es *ElasticSuite) TestUpdateProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(UPDATE_PRODUCT_SUCCESS_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	res, err := client.IndexProduct(ctx, m.Product{ID: 101})

	assert.Nil(es.T(), err, "Error should be nil")
	assert.Equal(es.T(), e.ELASTIC_UPDATE_DOC_RESPONSE, res, "result should be `updated`")
}

func (es *ElasticSuite) TestRetrieveAllProductOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	server, client, err := instantiate(RETRIEVE_ALL_PRODUCT_FOUND_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.RetrieveAllProduct(ctx, m.Query{})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")

}

func (es *ElasticSuite) TestRetrieveAllProductOnError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(GENERAL_ERROR_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.RetrieveAllProduct(ctx, m.Query{})

	assert.NotNil(es.T(), err, "Error shouldn't be nil")
}

func (es *ElasticSuite) TestRetrieveAllProductOnFoundBlankQuery() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(RETRIEVE_ALL_PRODUCT_FOUND_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.RetrieveAllProduct(ctx, m.Query{})

	assert.Nil(es.T(), err, "Error should be nil")
}

func (es *ElasticSuite) TestRetrieveAllProductOnFoundWithQuery() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(RETRIEVE_ALL_PRODUCT_FOUND_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.RetrieveAllProduct(ctx, m.Query{Keyword: "laptop", Filter: []string{"deleted", "active", "draft"}})

	assert.Nil(es.T(), err, "Error should be nil")
}

func (es *ElasticSuite) TestRetrieveAllProductOnFoundWithSpacedKeyword() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	server, client, err := instantiate(RETRIEVE_ALL_PRODUCT_FOUND_RESPONSE)
	defer server.Close()

	assert.Nil(es.T(), err, "Error should be nil")

	_, err = client.RetrieveAllProduct(ctx, m.Query{Keyword: "tas cantik", Filter: []string{"deleted", "active", "draft"}})

	assert.Nil(es.T(), err, "Error should be nil")
}
