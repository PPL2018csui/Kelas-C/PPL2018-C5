// Vue test utils: https://github.com/eddyerburgh/avoriaz
import Vue from 'vue';
import { shallow, mount } from 'avoriaz';
import Search from '../components/Search';

describe('Components ', () => {
  let cmp
  beforeEach(() => {
    cmp = mount(Search)
  })

  it('should show Search Component', () => {
    expect(cmp).toBeTruthy()
  })

  it('shows input', () => {
    expect(cmp.contains('input')).toBe(true)
  })

  it('shows button', () => {
    expect(cmp.contains('button')).toBe(true)
  })

  it('shows icon', () => {
    expect(cmp.contains('i')).toBe(true)
  })
  
})