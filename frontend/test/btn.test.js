// Vue test utils: https://github.com/eddyerburgh/avoriaz
import Vue from 'vue';
import { mount, shallow } from 'avoriaz';
import Button from '../components/Button';

describe('Components ', () => {
  let cmp
  let run = false;

  function testRunFn() {
      return run = true;
  }
    cmp = mount(Button, {
      propsData: {
          onClick: testRunFn
      }
    })

  it('should show something', () => {
    // To render DOM example: cmp.html()
    expect(cmp).toBeTruthy()
  })

  it('run function on click', () => {
    // To render DOM example: cmp.html()
    cmp.trigger('click')
    expect(run).toBe(true)
  })

  it('it has class orange if added props color orange', () => {
    cmp = mount(Button, {
      propsData: {
        onClick: testRunFn,
        color: "orange"
      }
    })
    expect(cmp.hasClass('orange')).toBe(true)
  })

  it('it has class green if added props color green', () => {
    cmp = mount(Button, {
      propsData: {
        onClick: testRunFn,
        color: "green"
      }
    })
    expect(cmp.hasClass('green')).toBe(true)
  })
})
