import axios from 'axios';

export const state = () => ({
  list: [],
  active: {},
  meta: {},
  products: [],
  error: null,
})

export const mutations = {
  SET_CATALOG (state, catalog) {
    state.active = catalog
  },
  SET_CATALOG_NAME (state, name) {
    state.active.name = name
  },
  SET_CATALOGS (state, catalog) {
    state.list = catalog
  },
  SET_METADATA (state, meta) {
    state.meta = meta
  },
  SET_FAILURE (state, error) {
    state.error = error
  },
  SET_PRODUCTS (state, products) {
    state.products = products
  },
  ADD_PRODUCT (state, {product, category}) {
    state.products.push({category: category, product: product})
  },
  REMOVE_PRODUCT (state, index) {
    state.products.splice(index,1)
  }
}

export const actions = {
  CREATE_CATALOG ({ commit }, catalog) {
    return axios.post(process.env.BASE_API_URL + '/catalog', catalog)
    .then((response) => {
      commit('SET_CATALOG', response.data.Body)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },

  GET_ALL_CATALOG ({ commit }, offset, limit) {
    return axios.get(process.env.BASE_API_URL + '/catalog', {params: {limit: limit, offset: offset}})
    .then((response) => {
      commit('SET_CATALOGS', response.data.Body)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },

  GET_CATALOG_BY_ID ({ commit }, id) {
    return axios.get(process.env.BASE_API_URL + '/catalog/' + id.toString())
    .then((response) => {
      commit('SET_CATALOG', response.data.Body)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },

  ADD_PRODUCT ({ commit }, product) {
    commit('ADD_PRODUCT', {product: product, category: product.category.name})
  },

  REMOVE_PRODUCT ({ commit }, index ) {
    commit('REMOVE_PRODUCT', index)
  },

  UPDATE_CATALOG ({ commit }, catalog) {
    return axios.put(process.env.BASE_API_URL + '/catalog', catalog)
    .then((response) => {
      commit('SET_CATALOG', response.data.Body)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  }
}
