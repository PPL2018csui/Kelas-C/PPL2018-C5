## Sikatan
[![pipeline status](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/badges/bulk_upload_product/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/commits/bulk_upload_product)
[![coverage report](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/badges/bulk_upload_product/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/commits/bulk_upload_product)

sistem katalog MAPAN

### Backend
- Using Golang and MySQL

#### Installation
- Install [Go][golang]
- Install [Docker][docker]
- Install [Govendor][govendor]
- run `make init`

#### Testing
- run `make test`

#### Contributing & Local Development
- install [Goimports][goimports]
- make your changes + test
- run `make start` and make sure the code is working in your local machine
- run `make test` and make sure no failure test and **the coverage is above 90%**
- **run `make pretty` before pushing your code**

### Frontend
- Using nuxt.js

#### Installation
``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

#### Testing
- make sure you've installed latest version of node.js
`$ npm run test`

#### Contributing
- to add image,video,and assets, add the file to frontend/assets
- to edit/add component, simply modify/add the <file>.vue file on frontend/components folder
- to edit/add pages, simply modify/add the <file>.vue file on frontend/pages folder
- to edit/add test, modify/add the <file>.test.js on frontend/test folder
- make your changes + test
- run `npm run test` and make sure no failure test

   [docker]: <https://www.docker.com/>
   [golang]: <https://golang.org/>
   [govendor]: <https://github.com/kardianos/govendor>
   [goimports]: <https://godoc.org/golang.org/x/tools/cmd/goimports>
